use num_cpus;
use rand::{prelude::thread_rng, Rng};
use std::thread;

pub fn expand(expand_text: Vec<&str>, sponge: bool, caps: bool) -> String {
    let cpu_count = num_cpus::get();
    let text_string = expand_text.join(" ");
    let text_string_length = text_string.chars().count();
    let worker_count = if cpu_count < text_string_length {
        cpu_count
    } else {
        text_string_length
    };

    let mut threads = vec![];
    for i in 0..worker_count {
        // Calculate the starting offset for the thread
        let start = i * (text_string_length / worker_count);
        // Calculate the end offset. If this is the last thread take up
        // any extra work on the end.
        let end = if i < worker_count - 1 {
            start + (text_string_length / worker_count)
        } else {
            text_string_length
        };
        let my_slice = String::from(&text_string[start..end]);

        threads.push(thread::spawn(move || {
            if sponge {
                sponge_transform(my_slice)
            } else {
                let expanded = classic_expand(&my_slice);
                if caps {
                    to_caps(expanded)
                } else {
                    expanded
                }
            }
        }));
    }

    let mut acc = String::from("");
    for thread in threads {
        acc.push_str(&thread.join().unwrap());
    }
    acc.trim().to_string()
}

fn classic_expand(expand_text: &str) -> String {
    let mut res = expand_text.split("").collect::<Vec<&str>>();
    res.remove(0);
    res.join(" ")
}

fn to_caps(expand_text: String) -> String {
    let mut capitalized = "".to_owned();
    for e in expand_text.chars() {
        for upper in e.to_uppercase() {
            capitalized.push(upper);
        }
    }

    capitalized
}

fn sponge_transform(to_sponge: String) -> String {
    let mut spongeified = "".to_owned();
    let mut rng = thread_rng();
    for s in to_sponge.chars() {
        let to_cap: f64 = rng.gen();
        if to_cap > 0.5 {
            for upper in s.to_uppercase() {
                spongeified.push(upper);
            }
        } else {
            for lower in s.to_lowercase() {
                spongeified.push(lower);
            }
        }
    }

    spongeified
}

#[cfg(test)]
mod tests {

    use crate::expander;

    #[test]
    fn classic_expand_works() {
        let result = expander::classic_expand(&"bubber".to_string());
        // classic_expands keeps trailing white spaces so that joining will work
        assert_eq!(result, "b u b b e r ");
    }
    #[test]
    fn no_trailing_whitespace_on_classic_expand() {
        let vec = vec!["bubber"];
        let result = expander::expand(vec, false, false);
        assert_eq!(result, "b u b b e r");
    }
    #[test]
    fn to_caps_works() {
        let result = expander::to_caps("bubber".to_string());
        assert_eq!(result, "BUBBER");
    }
}
